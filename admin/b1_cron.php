<?php

// Configuration
if (is_file('config.php')) {
    require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
    header('Location: ../install/index.php');
    exit;
}
require_once(DIR_SYSTEM . 'library/vendor/b1/libb1/B1.php');
require_once(DIR_SYSTEM . 'library/request.php');
require_once(DIR_SYSTEM . 'library/db.php');
require_once(DIR_SYSTEM . 'library/db/' . DB_DRIVER . '.php');
require_once(DIR_SYSTEM . 'library/config.php');
require_once(DIR_SYSTEM . 'engine/event.php');
require_once(DIR_SYSTEM . 'engine/loader.php');
require_once(DIR_SYSTEM . 'engine/model.php');
require_once(DIR_SYSTEM . 'engine/registry.php');
if (is_file(DIR_SYSTEM . 'engine/proxy.php')) {
    require_once(DIR_SYSTEM . 'engine/proxy.php');
}

class B1Cron
{

    const TTL = 3600;
    const MAX_ITERATIONS = 100;
    const ORDERS_PER_ITERATION = 100;
    const ORDER_SYNC_THRESHOLD = 10;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var DB
     */
    private $db;

    /**
     * @var ModelB1Settings
     */
    private $settings;

    /**
     * @var ModelB1Helper
     */
    private $helper;

    /**
     * @var ModelB1Items
     */
    private $items;

    /**
     * @var ModelB1Items
     */
    private $manager;
    /**
     * @var ModelB1Orders
     */

    private $orders;


    private function init()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        // Registry
        $registry = new Registry();
        // Config
        $config = new Config();
        $registry->set('config', $config);
        // Event
        $event = new Event($registry);
        $registry->set('event', $event);
        // Loader
        $loader = new Loader($registry);
        $registry->set('load', $loader);
        // Request
        $registry->set('request', new Request());
        // Database
        $registry->set('db', new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE));


        $this->request = $registry->get('request');
        $this->db = $registry->get('db');

        $loader->model('b1/settings');
        $this->settings = $registry->get('model_b1_settings');
        $loader->model('b1/helper');
        $this->helper = $registry->get('model_b1_helper');
        $loader->model('b1/items');
        $this->items = $registry->get('model_b1_items');
        $loader->model('b1/orders');
        $this->orders = $registry->get('model_b1_orders');
        $loader->model('b1/manager');
        $this->manager = $registry->get('model_b1_manager');

    }

    private function validateAccess()
    {
        $cron_key = $this->settings->get('cron_key');
        if ($cron_key == null) {
            throw new B1CronException('Fatal error.');
        }
        if (isset($this->request->get['key'])) {
            $get_key = $this->request->get['key'];
        } else {
            $get_key = null;
        }
        if ($get_key != $cron_key) {
            exit();
        }
    }

    public function __construct()
    {
        $this->init();
        $this->validateAccess();

        if (isset($this->request->get['id'])) {
            $get_id = $this->request->get['id'];
        } else {
            $get_id = null;
        }

        switch ($get_id) {
            case 'products':
                $this->manager->fetchProducts(true);
                break;
            case 'orders':
                $this->manager->syncOrders();
                break;
            case 'importProducts':
                $this->manager->getImportDropdownItems();
                break;
            default:
                throw new B1Exception('Bad action ID specified.');
        }
    }

}

new B1Cron;