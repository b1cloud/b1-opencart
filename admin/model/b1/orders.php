<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Orders extends ModelB1Base
{
    public function assignB1OrderId($b1_order, $order_id)
    {
        if (!$b1_order){
            return $this->db->query("UPDATE " . DB_PREFIX . "order SET b1_reference_id = NULL WHERE order_id = " . $this->db->escape($order_id));
        } else {
            return $this->db->query("UPDATE " . DB_PREFIX . "order SET b1_reference_id = " . $this->db->escape($b1_order) . " WHERE order_id = " . $this->db->escape($order_id));
        }


    }
    public function getOrdersForSync($iterations, $order_status_id, $orders_sync_from)
    {
        return $this->db->query("SELECT DISTINCT o.order_id as int_id, o.order_id, o.currency_code, o.currency_value, o.total, o.email, o.payment_company, o.payment_firstname, o.payment_lastname, o.payment_address_1, o.payment_city, o.shipping_firstname, o.shipping_lastname, o.shipping_address_1, o.shipping_city, o.shipping_company, c.*, oh.order_history_id,oh.date_added FROM " . DB_PREFIX . "order o
                    INNER JOIN " . DB_PREFIX . "country c ON o.payment_country_id = c.country_id
                    inner join (
                        SELECT  order_id,
                                MAX( order_history_id) MaxId
                        FROM    " . DB_PREFIX . "order_history
                        where  order_status_id = '" . $order_status_id . "'
                        GROUP BY order_id
                    ) MaxHistIds on MaxHistIds.order_id = o.order_id 
                    INNER JOIN " . DB_PREFIX . "order_history AS oh on o.order_id = oh.order_id
                    WHERE  b1_reference_id IS NULL AND oh.order_status_id = '" . $order_status_id . "' AND oh.date_added >= '" . $orders_sync_from . "' AND oh.order_history_id = MaxHistIds.MaxId ORDER BY oh.date_added ASC LIMIT " . $iterations);
    }

    public function getOrderStatuses($languageCode)
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_status` WHERE language_id = '" . $languageCode . "' ORDER BY order_status_id");
        return $query->rows;
    }

    public function getDataBySql($sql, $id)
    {
        if ($sql) {
            foreach ($this->db->query(str_replace('%d', $id, $sql))->row as $row) {
                return $row;
            }
        }
    }

}