<?php
/**
 * Created by PhpStorm.
 * User: JohnMirro
 * Date: 23.09.2021
 * Time: 12:56
 */

class ModelB1Logs
{
    const PER_PAGE = 20;

    public function __construct()
    {
        // Registry
        $registry = new Registry();
        // Database
        $registry->set('db', new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE));

        $this->request = $registry->get('request');
        $this->db = $registry->get('db');

    }

    public static function tableName()
    {
        return DB_PREFIX . 'b1_logs';
    }

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . self::tableName() . "` (
                id bigint(11) NOT NULL AUTO_INCREMENT,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                is_success smallint,
                debug_info text,
                PRIMARY KEY  (id) 
            ) ENGINE=MYISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
    }

    public function deleteTable()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . self::tableName() . "`");
    }

    /**
     * Used by cron to clear log items older than 30 days
     */
    public function clearOldLogs()
    {
        try {
            $time = date('Y-m-d H:i:s', strtotime('-7 day'));
            $tableName = self::tableName();

            $this->db->query("DELETE FROM {$tableName} WHERE `created_at` < '{$time}'");

        } catch (Exception $e) {
        }
    }

    /**
     * Save debug info to log
     * @param $is_success
     * @param $debug_info
     */
    public function save($is_success, $debug_info)
    {
        try {
            $tableName = self::tableName();
            $this->db->query("INSERT INTO `{$tableName}`(`is_success`, `debug_info`) VALUES('" . $this->db->escape($is_success) . "', '" . $this->db->escape(json_encode($debug_info, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)) . "')");
        } catch (Exception $e) {
        }
    }

    public function fetchAllLogsAsHtml($page = 1)
    {
        $html = '<table class="table table-hover B1-logs">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Request detail</th>
                        </tr>
                        </thead>
                        <tbody >';

        $this->clearOldLogs(); // we are not have normal cron in opencart2

        try {
            $per_page = self::PER_PAGE;
            $offset = ($page - 1) * $per_page;

            $tableName = self::tableName();

            $sql = "SELECT * FROM {$tableName} ORDER BY `created_at` DESC LIMIT {$per_page} OFFSET {$offset}";

            $total_query = $this->db->query("SELECT COUNT(*) as total FROM {$tableName}")->row;
            $total = $total_query['total'];
            $num_of_pages = ceil($total / $per_page);

            $logs = $this->db->query($sql);

            foreach ($logs->rows as $key => $row) {
                $short_text = substr($row['debug_info'], 0, 150);
                $badge = $row['is_success'] == 1 ? '<span class="label label-success">OK</span>' : '<span class="label label-danger">Error</span>';

                $html .= "<tr data-id='{$row['id']}'>
                                <td><input type=\"checkbox\" name=\"b1LogItemChkBox[]\" value=\"{$row['id']}\"></td>
                                <td>{$row['created_at']}</td>
                                <td>{$badge}</td>
                                <td>{$short_text}</td>
                            </tr>";
            }

            // pagination
            $range = 2; /* how many pages around page selected */
            $start = (int)($page - $range);
            if ($start < 1) {
                $start = 1;
            }
            $stop = (int)($page + $range);
            if ($stop > $num_of_pages) {
                $stop = (int)($num_of_pages);
            }
            if ($total > 0) {
                $page_links = $this->getPaginationHtml($page, $start, $stop, $num_of_pages);
            } else {
                $page_links = '';
            }
        } catch (\Exception $e) {
        }
        $html .= '</tbody></table>
                <div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
        return $html;
    }

    /**
     * Return one log item to view
     * @param $id
     * @return string
     */
    public function fetchLogByIdAsHtml($id)
    {
        $html = '';
        try {
            $tableName = self::tableName();
            $log_item = $this->db->query("SELECT * FROM {$tableName} WHERE `id` = {$id}")->row;

            if ($log_item !== null) {

                $badge = $log_item['is_success'] == 1 ? '<span class="label label-success">OK</span>' : '<span class="label label-danger">Error</span>';
                $html .= "<div class=\"panel panel-default\">
                            <div class=\"panel-heading\">
                               #{$log_item['id']} {$badge} at {$log_item['created_at']}
                            </div>
                            <div class=\"panel-body\">
                                <div class=\"form-group\">
                                    <textarea style=\"font-family:monospace;\" rows=\"60\" cols=\"100\" readonly>{$log_item['debug_info']}</textarea>
                                </div>
                            </div>
                      </div>";
            }

        } catch (\Exception $e) {
        }
        return $html;
    }


    /**
     * Export log to file
     * @param $selected_ids
     */
    public function exportLogs($selected_ids)
    {
        try {
            $result = [];
            $where_clause = "";
            if (strlen($selected_ids) > 0) {
                $where_clause = "WHERE `id` in (" . $selected_ids . ")";
            }
            $tableName = self::tableName();
            $sql = "SELECT `created_at`,`is_success`,`debug_info` FROM {$tableName} {$where_clause}";
            $logs = $this->db->query($sql);

            foreach ($logs->rows as $key => $row) {
                $debug_info = json_decode($row['debug_info'], true);
                $is_success = $row['is_success'] == 1;
                $result[] = [
                    'created_at' => $row['created_at'],
                    'is_success' => $is_success,
                    'debug_info' => $debug_info
                ];

            }

            return json_encode($result, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT);
        } catch (Exception $e) {
        }

    }

    /**
     * Custom pagination generator
     * @param $p
     * @param $n
     * @param $start
     * @param $stop
     * @param $pages_nb
     * @return string
     */
    private function getPaginationHtml($p, $start, $stop, $pages_nb)
    {
        $html = '<div id="pagination" class="pagination">';

        if ($start != $stop) {
            $html .= '<ul class="pagination">';
            if ($p !== 1) {
                $p_previous = $p - 1;
                $html .= '<li id="pagination_previous"><a rel="' . $p_previous . '" class="pagination_page_number" href="#">&laquo;&nbsp;Previous</a></li>';
            } else {
                $html .= '<li id="pagination_previous" class="disabled"><span>&laquo;&nbsp;Previous</span></li>';
            }
            if ($start > 3) {
                $html .= '<li><a rel="1" class="pagination_page_number" href="#">1</a></li>
                    <li class="truncate">...</li>';
            }
            for ($i = $start; $i < $stop + 1; $i++) {
                if ($p == $i) {
                    $html .= '<li class="current"><span>' . $p . '</span></li>';
                } else {
                    $html .= '<li><a rel="' . $i . '" class="pagination_page_number" href="#">' . $i . '</a></li>';
                }
            }
            if ($pages_nb > $stop + 2) {
                $html .= '<li class="truncate">...</li>
                    <li><a rel="' . $pages_nb . '" class="pagination_page_number" href="#">' . $pages_nb . '</a></li>';
            }
            if ($pages_nb > 1 && $p !== $pages_nb) {
                $p_next = $p + 1;
                $html .= '<li id="pagination_next"><a class="pagination_page_number" rel="' . $p_next . '" href="#">Next&nbsp;&raquo;</a></li>';
            } else {
                $html .= '<li id="pagination_next" class="disabled"><span>Next&nbsp;&raquo;</span></li>';
            }
            $html .= '</ul>';
        }
        $html .= '</div>';
        return $html;
    }


}
