<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Settings extends ModelB1Base
{

    public static function tableName()
    {
        return DB_PREFIX . 'b1_settings';
    }

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . self::tableName() . "` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `key` varchar(128) NOT NULL,
            `value` text NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=MYISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
    }

    public function deleteTable()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . self::tableName() . "`");
    }

    public function insertDefaultSettings()
    {
        //configuration
        $this->add('cron_key', $this->generateCronKey());
        $this->add('api_key', '');
        $this->add('private_key', '');
        $this->add('shop_id', base_convert(time(), 10, 36));
        $this->add('b1_id', '');
        $this->add('documentation_url', 'https://www.b1.lt/doc/api');
        $this->add('help_page_url', 'https://www.b1.lt/help');
        $this->add('b1_contact_email', 'info@b1.lt');
        $this->add('orders_sync_from', date("Y-m-d"));
        $this->add('order_status_id', 5);
        $this->add('quantity_sync', 0);
        $this->add('sync_item_name', 0);
        $this->add('sync_item_price', 0);
        $this->add('vat_rate', 0);
        $this->add('sync_invoice', 0);
        $this->add('vat_rate', 0);
        $this->add('write_off', 0);


        // mapping
        $this->add('order_date', '
SELECT o1.date_added 
FROM ' . DB_PREFIX . 'order o1 
LEFT JOIN ' . DB_PREFIX . 'order_history o2 on o2.order_id = o1.order_id
where o1.order_id = %d
        ');
        $this->add('order_no', '
SELECT o1.order_id
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('invoice_number', '
SELECT LPAD(o1.invoice_no,6,0)
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('invoice_series', '
SELECT o1.invoice_prefix 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('currency', '
SELECT o1.currency_code 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('shipping_amount', '
SELECT o2.value 
FROM ' . DB_PREFIX . 'order o1
LEFT JOIN  ' . DB_PREFIX . 'order_total o2 on o2.order_id = o1.order_id
where o2.code = ' . '\'shipping\'' . ' AND o1.order_id = %d 
        ');
        $this->add('discount', '
SELECT o2.value 
FROM ' . DB_PREFIX . 'order o1
LEFT JOIN  ' . DB_PREFIX . 'order_total o2 on o2.order_id = o1.order_id
where o2.code = ' . '\'coupon\'' . ' AND o1.order_id = %d 
        ');
        $this->add('gift', '');
        $this->add('total', '
SELECT o2.value 
FROM ' . DB_PREFIX . 'order o1
LEFT JOIN  ' . DB_PREFIX . 'order_total o2 on o2.order_id = o1.order_id
where o2.code = ' . '\'total\'' . ' AND o1.order_id = %d 
        ');
        $this->add('order_email', '
SELECT o1.email 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
//billing
        $this->add('billing_is_company', '
SELECT o1.payment_company 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('billing_first_name', '
SELECT o1.payment_firstname 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('billing_last_name', '
SELECT o1.payment_lastname 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('billing_address', '
SELECT o1.payment_address_1 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('billing_city', '
SELECT o1.payment_city 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('billing_country', '
SELECT o2.iso_code_2
FROM ' . DB_PREFIX . 'order o1 
LEFT JOIN  ' . DB_PREFIX . 'country o2 on o1.payment_country_id = o2.country_id
where o1.order_id = %d
        ');
        $this->add('billing_short_name', '');
        $this->add('billing_postcode', '');
        $this->add('billing_vat_code', '');
        $this->add('billing_code', '');
//delivery
        $this->add('delivery_is_company', '
SELECT o1.shipping_company 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('delivery_first_name', '
SELECT o1.shipping_firstname 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('delivery_last_name', '
SELECT o1.shipping_lastname 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('delivery_address', '
SELECT o1.shipping_address_1 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('delivery_city', '
SELECT o1.shipping_city 
FROM ' . DB_PREFIX . 'order o1 
where o1.order_id = %d
        ');
        $this->add('delivery_country', '
SELECT o2.iso_code_2
FROM ' . DB_PREFIX . 'order o1 
LEFT JOIN  ' . DB_PREFIX . 'country o2 on o1.shipping_country_id = o2.country_id
where o1.order_id = %d
        ');
        $this->add('delivery_short_name', '');
        $this->add('delivery_vat_code', '');
        $this->add('delivery_postcode', '');
        $this->add('delivery_code', '');

//payment
        $this->add('payer_name', '');
        $this->add('payer_code', '');
        $this->add('payer_vat_code', '');
        $this->add('payer_address', '');
        $this->add('payer_country_code', '');
        $this->add('payment_code', '');
        $this->add('payment_id', '');
        $this->add('payment_payment_date', '');
        $this->add('payment_sum', '');
        $this->add('payment_tax', '');
        $this->add('payment_currency', '');
        $this->add('payment_payment', '');
//items   order_product_id

        $this->add('items_name', '
SELECT o1.name
FROM ' . DB_PREFIX . 'order_product o1 
where o1.order_product_id = %d
        ');
        $this->add('items_quantity', '
SELECT o1.quantity
FROM ' . DB_PREFIX . 'order_product o1 
where o1.order_product_id = %d
        ');
        $this->add('items_price', '
SELECT o1.price
FROM ' . DB_PREFIX . 'order_product o1 
where o1.order_product_id = %d
        ');
        $this->add('items_sum', '
SELECT o1.total
FROM ' . DB_PREFIX . 'order_product o1 
where o1.order_product_id = %d
        ');
        $this->add('items_vat_rate', '');

    }

    private function generateCronKey()
    {
        return hash_hmac('sha256', uniqid(rand(), true), microtime() . rand());
    }

    public function updateExternalTables()
    {
        $this->db->query("ALTER TABLE `" . self::productTableName() . "` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL");
        $this->db->query("ALTER TABLE `" . self::orderTableName() . "` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL");
        $this->load->model('user/user_group');
        $this->model_user_user_group->addPermission($this->user->getId(), 'access', 'module/b1');
        $this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'module/b1');
    }

    public function deleteExternalTableModifications()
    {
        $this->db->query("ALTER TABLE `" . self::productTableName() . "` DROP `b1_reference_id`");
        $this->db->query("ALTER TABLE `" . self::orderTableName() . "` DROP `b1_reference_id`");
        $this->load->model('user/user');
        $this->load->model('user/user_group');
        $user = $this->model_user_user->getUser($this->user->getId());
        $userGroupData = $this->model_user_user_group->getUserGroup($user['user_group_id']);
        unset($userGroupData['access']['module/b1']);
        unset($userGroupData['modify']['module/b1']);
        $this->model_user_user_group->editUserGroup($user['user_group_id'], $userGroupData);
    }

    public function get($key)
    {
        $query = $this->db->query("SELECT * FROM `" . self::tableName() . "` WHERE `key` = '" . $this->db->escape($key) . "'");
        if (isset($query->row['value'])) {
            return $query->row['value'];
        } else {
            return null;
        }
    }

    public function set($key, $data)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '" . $this->db->escape($data) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function update($keys, $data)
    {
        foreach ($keys as $key) {
            $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '" . $this->db->escape($data[$key]) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
        }
    }

    public function delete($key)
    {
        $this->db->query("DELETE FROM `" . self::tableName() . "` WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function add($key, $value)
    {
        $this->db->query("INSERT INTO `" . self::tableName() . "` SET `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
    }

    public function disable($key)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '0' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function enable($key)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '1' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function resetOrdersId()
    {
        $this->db->query("UPDATE `" . self::orderTableName() . "` SET `b1_reference_id` = null where `b1_reference_id` is not null");
    }
}
