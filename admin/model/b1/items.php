<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Items extends ModelB1Base
{

    public function resetAllB1ReferenceId()
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET b1_reference_id = NULL ");

    }

    public function updateProductReferenceId($id, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET b1_reference_id = " . $id . " WHERE sku ='" . $code . "'");

    }

    public function updateProductQuantity($quantity, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . $quantity . "' WHERE b1_reference_id = " . $code);

    }

    public function updateItemName($name, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product_description as pd
        left join " . DB_PREFIX . "product as p on pd.product_id = p.product_id
        SET pd.name = '" . $this->db->escape($name) . "' WHERE p.b1_reference_id = " . $code . "");

    }

    public function updateItemPrice($price, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET price = '" . $price . "' WHERE b1_reference_id = " . $code);
    }

    public function checkShopVatSettings()
    {
        return $this->db->query("SELECT * FROM `" . DB_PREFIX . "setting` where `key` = 'config_tax'")->row;
    }

    public function fetchAllItems($interactions)
    {
        $query = $this->db->query("
SELECT * FROM " . DB_PREFIX . "product p
LEFT JOIN " . DB_PREFIX . "product_description pd ON p.product_id = pd.product_id
WHERE b1_reference_id IS NULL AND p.sku is not null AND p.sku != ' ' LIMIT " . $interactions);
        return $query->rows;
    }

}
