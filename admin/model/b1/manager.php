<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class ModelB1Manager
{

    const TTL = 3600;
    const MAX_ITERATIONS = 100;
    const ORDERS_PER_ITERATION = 100;
    const ITEMS_PER_ITERATION = 100;
    const ORDER_SYNC_THRESHOLD = 10;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var DB
     */
    private $db;

    /**
     * @var ModelB1Settings
     */
    private $settings;

    /**
     * @var ModelB1Helper
     */
    private $helper;

    /**
     * @var ModelB1Items
     */
    private $items;

    /**
     * @var ModelB1Orders
     */

    private $orders;
    /**
     * @var B1
     */
    private $library;


    public function __construct()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        // Registry
        $registry = new Registry();
        // Config
        $config = new Config();
        $registry->set('config', $config);
        // Event
        $event = new Event($registry);
        $registry->set('event', $event);
        // Loader
        $loader = new Loader($registry);
        $registry->set('load', $loader);
        // Request
        $registry->set('request', new Request());
        // Database
        $registry->set('db', new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE));

        $this->request = $registry->get('request');
        $this->db = $registry->get('db');

        $loader->model('b1/settings');
        $this->settings = $registry->get('model_b1_settings');
        $loader->model('b1/helper');
        $this->helper = $registry->get('model_b1_helper');
        $loader->model('b1/items');
        $this->items = $registry->get('model_b1_items');
        $loader->model('b1/orders');
        $this->orders = $registry->get('model_b1_orders');
    }

    /**
     * @return array
     * @throws B1Exception
     */

    public function getImportDropdownItems()
    {
        $this->library = new B1([
            'apiKey' => $this->settings->get('api_key'),
            'privateKey' => $this->settings->get('private_key')
        ]);

        $data = [
            'page' => 1,
            'rows' => 100,
            'filters' => [
                'groupOp' => 'AND',
            ],
        ];
        $resultAttributeId = $this->library->request('reference-book/item-attributes/list', $data);
        $resultMeasurementId = $this->library->request('reference-book/measurement-units/list', $data);
        $AttributeId = $resultAttributeId->getContent();
        $MeasurementId = $resultMeasurementId->getContent();
        return [
            'attributeId' => $AttributeId,
            'measurementId' => $MeasurementId,
        ];
    }

    public function fetchProducts()
    {
       $this->library = new B1([
            'apiKey' => $this->settings->get('api_key'),
            'privateKey' => $this->settings->get('private_key')
        ]);
        $i = 0;
        $enableQuantitySync = $this->settings->get('quantity_sync');
        $enableSyncItemName = $this->settings->get('sync_item_name');
        $enableSyncItemPrice = $this->settings->get('sync_item_price');
        $this->items->resetAllB1ReferenceId();

        $page = 0;
        $response = $this->library->request('e-commerce/config/get');
        $content = $response->getContent();
        $warehouseId = $content['data']['warehouseId'];

        do {
            $i++;
            try {
                $response = $this->library->request('e-commerce/items/stock', [
                    'warehouseId' => $warehouseId,
                    'page' => ++$page,
                    'pageSize' => 100,
                    'filters' => [
                        'groupOp' => 'AND',
                        'rules' => [
                        ],
                    ],
                ]);
                $data = $response->getContent();

                foreach ($data['data'] as $item) {
                    if ($item['code'] != NULL) {
                        $this->items->updateProductReferenceId($item['id'], $item['code']);
                        if ($enableQuantitySync == 1) {
                            $this->items->updateProductQuantity($item['quantity'], $item['id']);
                        }
                        if ($enableSyncItemName == 1) {
                            $this->items->updateItemName($item['name'], $item['id']);
                        }
                        if ($enableSyncItemPrice == 1 && $item['priceWithoutVat'] > 0) {
                            $this->items->updateItemPrice($item['priceWithoutVat'], $item['id']);
                        }
                    }
                }

            } catch (B1Exception $e) {
                $this->helper->printPre($e->getMessage());
                $this->helper->printPre($e->getExtraData());
            } catch (B1Exception $e) {
                $content = $e->getResponse()->getContent();
                $this->helper->printPre($content['message']);
            } catch (B1DuplicateException $e) {
                $content = $e->getResponse()->getContent();
                $this->helper->printPre($content['message']);
            } catch (B1DuplicateException $e) {
                $content = $e->getResponse()->getContent();
                $this->helper->printPre($content['message']);
            } catch (B1ValidationException $e) {
                $content = $e->getResponse()->getContent();
                $this->helper->printPre($content['message']);
            } catch (B1ResourceNotFoundException $e) {
                $content = $e->getResponse()->getContent();
                $this->helper->printPre($content['message']);
            } catch (B1InternalErrorException $e) {
                $content = $e->getResponse()->getContent();
                $this->helper->printPre($content['message']);
            }
            echo "$i;";
        } while ($page <= $data['pages'] && $i < self::MAX_ITERATIONS);
        echo 'OK';
    }
    public function print_error_data($e, $order)
    {
        $content = $e->getResponse()->getContent();
        echo "<pre style='color:#ff0000'>";
        print_r([
            'errors' => $content['errors'],
            'message' => $content['message'],
            'order' => $order,
        ]);
        echo '</pre>';
    }

    public function delete_order($orderId, $shopId)
    {
        $data = [
            'internalId' => $orderId,
            'shopId' => $shopId
        ];
        $response = $this->library->request('e-commerce/orders/delete', $data);
    }
    public function syncOrders()
    {
        $this->library = new B1([
            'apiKey' => $this->settings->get('api_key'),
            'privateKey' => $this->settings->get('private_key')
        ]);
        try {
            $orders_sync_from = $this->settings->get('orders_sync_from');
            if ($orders_sync_from == null) {
                throw new B1Exception('Not set orders_sync_from value');
            }
            $order_status_id = $this->settings->get('order_status_id');
            if ($order_status_id == null) {
                throw new B1Exception('Not set orders_status_id value');
            }
            $data_prefix = $this->settings->get('shop_id');
            if ($data_prefix == null) {
                throw new B1Exception('Not set shop_id value');
            }
            $enableQuantitySync = $this->settings->get('quantity_sync');

            $enableWriteOff = $this->settings->get('write_off');

            $test = $this->settings->get('order_date');
            if (!$test || trim($test) == "")
                throw new B1Exception("Empty field mapping values (order_date). You must again enable B1 plugin (disable first, if it is enabled).");

            $i = 0;
            do {
                $i++;
                $processed = 0;
                $orders = $this->orders->getOrdersForSync(self::ORDERS_PER_ITERATION, $order_status_id, $orders_sync_from);
                foreach ($orders->rows as $order) {
                    $taxRate = $this->getTaxRate($order['order_id']);
                    $order_data = $this->generateOrderData($order, $order_status_id, $data_prefix, $taxRate);
                    $items = $this->generateOrderItemData($order, $taxRate);
                    if ($order_data != 'skip') {
                        try {
                            $response = $this->library->request('e-commerce/orders/create', $order_data);
                            $request = $response->getContent();
                            if (!isset($request['data']['id'])) {
                                print_r($request);
                                throw new Exception("Bad response from B1");
                            }
                            $orderId = $request['data']['id'];
                            $this->orders->assignB1OrderId($orderId, $order['order_id']);
                        } catch (B1DuplicateException $e) {
                            $content = $e->getResponse()->getContent();
                            $this->orders->assignB1OrderId($content['data']['id'], $order['order_id']);
                            print_r($content['message']);
                            continue;
                        } catch (B1ValidationException $e) {
                            $content = $e->getResponse()->getContent();
                            echo '<pre>';
                            print_r([
                                'errors' => $content['errors'],
                                'message' => $content['message'],
                                'order' => $order,
                            ]);
                            echo "</pre>";
                            exit;
                        }
                        try {
                            if ($items == 'skip'){
                                echo "<br>Error with order ".$order['order_id']." items <br>";
                                $this->delete_order($order['order_id'], $order_data['shopId']);
                                $this->orders->assignB1OrderId(null, $order['order_id']);
                                continue; // Ticket #18620
                            }
                            $ids = [];
                            foreach ($items as $orderItemData) {
                                $orderItemData['orderId'] = $orderId;
                                $response = $this->library->request('e-commerce/order-items/create', $orderItemData);
                                $ids[] = $orderItemData['itemId'];
                            }
                            if ($enableWriteOff) {
                                $data = [
                                    'orderId' => $orderId,
                                ];
                                $response = $this->library->request('e-commerce/orders/create-write-off', $data);
                            }
                            $data = [
                                'orderId' => $orderId,
                                'series' => isset($order_data['customSeries']) ? $order_data['customSeries'] : null,
                                'number' => isset($order_data['customNumber']) ? $order_data['customNumber'] : null,
                            ];
                            $response = $this->library->request('e-commerce/orders/create-sale', $data);
                            if ($enableQuantitySync) {
                                $response = $this->library->request('e-commerce/config/get');
                                $content = $response->getContent();
                                $warehouseId = $content['data']['warehouseId'];

                                $page = 0;
                                do {
                                    $data = [
                                        'warehouseId' => $warehouseId,
                                        'page' => ++$page,
                                        'pageSize' => 100,
                                        'filters' => [
                                            'groupOp' => 'AND',
                                            'rules' => [
                                                [
                                                    'field' => 'id',
                                                    'op' => 'in',
                                                    'data' => $ids
                                                ]
                                            ],
                                        ],
                                    ];
                                    $response = $this->library->request('e-commerce/items/stock', $data);
                                    $content = $response->getContent();
                                    $retStocks = $content['data'];
                                    foreach ($retStocks as $stock) {
                                        if (isset($stock['id']) && isset($stock['quantity'])) {
                                            $this->items->updateProductQuantity($stock['quantity'], $stock['id']);
                                        }
                                    }
                                } while ($content['pages'] > $page);
                            }
                        } catch (B1DuplicateException $e) {
                            $content = $e->getResponse()->getContent();
                            $this->orders->assignB1OrderId($content['data']['id'], $order['order_id']);
                            print_r($content['message']);
                        } catch (B1ValidationException $e) {
                            $this->print_error_data($e, $order);
                            $this->delete_order($order['order_id'], $order_data['shopId']);
                            $this->orders->assignB1OrderId(null, $order['order_id']);
                            exit;
                        } catch (B1ResourceNotFoundException $e) {
                            $this->print_error_data($e, $order);
                            $this->delete_order($order['order_id'], $order_data['shopId']);
                            $this->orders->assignB1OrderId(null, $order['order_id']);
                            exit;
                        } catch (B1InternalErrorException $e) {
                            $this->print_error_data($e, $order);
                            $this->delete_order($order['order_id'], $order_data['shopId']);
                            $this->orders->assignB1OrderId(null, $order['order_id']);
                            exit;
                        } catch (Exception $e) {
                            $this->print_error_data($e, $order);
                            $this->delete_order($order['order_id'], $order_data['shopId']);
                            $this->orders->assignB1OrderId(null, $order['order_id']);
                            exit;
                        }
                    }

                    $processed++;
                    echo "$i-$processed; ";
                }
            } while (count($orders->rows) == self::ORDERS_PER_ITERATION && $i < self::MAX_ITERATIONS);
            echo 'OK';
        } catch (B1Exception $e) {
            print_r(json_encode($e->getMessage()));
            die();
        }
    }

    public function getTaxRate($orderId)
    {
        $taxAddresses = ["shipping", "payment"]; // Selecting taxes base on shipping or payment address
        foreach ($taxAddresses as $taxAddress) {
            $tax_query = $this->db->query("
            SELECT tr1.tax_class_id, tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority 
            FROM " . DB_PREFIX . "tax_rule tr1 
            LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) 
            INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) 
            LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) 
            LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id)
            INNER JOIN  " . DB_PREFIX . "order o on (  z2gz.country_id = o." . $this->db->escape($taxAddress) . "_country_id AND (z2gz.zone_id = '0' OR z2gz.zone_id = o." . $this->db->escape($taxAddress) . "_zone_id))
            WHERE tr1.based = '" . $this->db->escape($taxAddress) . "'  AND o.order_id = " . $this->db->escape($orderId) . " AND tr2.type = 'P' ORDER BY tr1.priority ASC");

            $taxRates = $tax_query->rows;
            if (empty($taxRates)) continue;
            break;
        }
        foreach ($taxRates as $rateArr) {
            if (!is_null($rateArr['rate'])){
                return floatval($rateArr['rate']);
            }
        }
        // Tax based on store address
        $tax_query = $this->db->query("
            SELECT tr1.tax_class_id, tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority 
            FROM " . DB_PREFIX . "tax_rule tr1 
            LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) 
            INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) 
            INNER JOIN  " . DB_PREFIX . "order o on tr2cg.customer_group_id = o.customer_group_id
            WHERE tr1.based = 'store'  AND o.order_id = " . $this->db->escape($orderId) . " AND tr2.type = 'P' ORDER BY tr1.priority ASC");
        $taxRates = $tax_query->rows;

        if (empty($taxRates)) return null;

        foreach ($taxRates as $rateArr) {
            if (!is_null($rateArr['rate'])){
                return floatval($rateArr['rate']);
            }
        }
        return null;
    }

    public function generateOrderData($item, $order_status_id, $data_prefix, $taxRate)
    {
        $order_status = $this->db->query("SELECT `date_added` FROM " . DB_PREFIX . "order_history WHERE `order_status_id` = " . $this->db->escape($order_status_id) . " AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        if (empty($order_status)) {
            throw new B1Exception('Not found order_history data, for ' . $this->db->escape($item['order_id']) . ' order.', array(
                'order_status_id' => $order_status_id,
                'order' => $item,
            ));
        }
        $order_data = array();
        $order_data['shopId'] = $data_prefix;
        $order_data['internalId'] = (int)$item['order_id'];
        $order_data['date'] = substr($this->orders->getDataBySql($this->settings->get('order_date'), $item['order_id']), 0, 10);
        $order_data['number'] = $this->orders->getDataBySql($this->settings->get('order_no'), $item['order_id']);
        if ($this->orders->getDataBySql($this->settings->get('invoice_series'), $item['order_id']) && $this->settings->get('sync_invoice')) {
            $order_data['customSeries'] = $this->orders->getDataBySql($this->settings->get('invoice_series'), $item['order_id']) ;
            $order_data['customNumber'] = $this->orders->getDataBySql($this->settings->get('invoice_number'), $item['order_id']);
        }
        $order_data['currencyCode'] = $this->orders->getDataBySql($this->settings->get('currency'), $item['order_id']);;
        $order_data['shipping'] = round($this->orders->getDataBySql($this->settings->get('shipping_amount'), $item['order_id']) *  $item['currency_value'] * (1 + $taxRate / 100),2);
        $order_data['discount'] = round(abs($this->orders->getDataBySql($this->settings->get('discount'), $item['order_id']) * (1 + $taxRate / 100) * $item['currency_value']),2) ;
        $order_data['total'] = round($this->orders->getDataBySql($this->settings->get('total'), $item['order_id']) * $item['currency_value'],2);
        $order_data['email'] = $this->orders->getDataBySql($this->settings->get('order_email'), $item['order_id']);;
        $order_data['vatRate'] = $this->settings->get('vat_rate');
        $order_data['writeOff'] = $this->settings->get('write_off');

        $billingCompany = $this->orders->getDataBySql($this->settings->get('billing_is_company'), $item['order_id']);
        $order_data['billing']['isJuridical'] = $billingCompany == '' ? 0 : 1;
        $order_data['billing']['name'] = empty($billingCompany) ? trim($this->orders->getDataBySql($this->settings->get('billing_first_name'), $item['order_id']) . ' ' . $this->orders->getDataBySql($this->settings->get('billing_last_name'), $item['order_id'])) : $billingCompany;
        $order_data['billing']['address'] = $this->orders->getDataBySql($this->settings->get('billing_address'), $item['order_id']);
        $order_data['billing']['cityName'] = $this->orders->getDataBySql($this->settings->get('billing_city'), $item['order_id']);
        $order_data['billing']['countryCode'] = $this->orders->getDataBySql($this->settings->get('billing_country'), $item['order_id']);
        //extra billing fields
        if ($this->settings->get('billing_vat_code')) {
            $order_data['billing']['vatCode'] = $this->orders->getDataBySql($this->settings->get('billing_vat_code'), $item['order_id']);
        }
        if ($this->settings->get('billing_short_name')) {
            $order_data['billing']['shortName'] = $this->orders->getDataBySql($this->settings->get('billing_short_name'), $item['order_id']);
        }
        if ($this->settings->get('billing_postcode')) {
            $order_data['billing']['postcode'] = $this->orders->getDataBySql($this->settings->get('billing_postcode'), $item['order_id']);
        }
        if ($this->settings->get('billing_code')) {
            $order_data['billing']['code'] = $this->orders->getDataBySql($this->settings->get('billing_code'), $item['order_id']);
        }

        $deliveryCompany = $this->orders->getDataBySql($this->settings->get('delivery_is_company'), $item['order_id']);
        $order_data['delivery']['isJuridical'] = $deliveryCompany== '' ? 0 : 1;
        $order_data['delivery']['name'] = empty($deliveryCompany) ? trim($this->orders->getDataBySql($this->settings->get('delivery_first_name'), $item['order_id']) . ' ' . $this->orders->getDataBySql($this->settings->get('delivery_last_name'), $item['order_id'])) : $deliveryCompany;
        $order_data['delivery']['address'] = $this->orders->getDataBySql($this->settings->get('delivery_address'), $item['order_id']);
        $order_data['delivery']['cityName'] = $this->orders->getDataBySql($this->settings->get('delivery_city'), $item['order_id']);
        $order_data['delivery']['countryCode'] = $this->orders->getDataBySql($this->settings->get('delivery_country'), $item['order_id']);
        //extra delivery fields
        if ($this->settings->get('delivery_vat_code')) {
            $order_data['delivery']['vatCode'] = $this->orders->getDataBySql($this->settings->get('delivery_vat_code'), $item['order_id']);
        }
        if ($this->settings->get('delivery_short_name')) {
            $order_data['delivery']['shortName'] = $this->orders->getDataBySql($this->settings->get('delivery_short_name'), $item['order_id']);
        }
        if ($this->settings->get('delivery_postcode')) {
            $order_data['delivery']['postcode'] = $this->orders->getDataBySql($this->settings->get('delivery_postcode'), $item['order_id']);
        }
        if ($this->settings->get('delivery_code')) {
            $order_data['delivery']['code'] = $this->orders->getDataBySql($this->settings->get('delivery_code'), $item['order_id']);
        }
        //
        if ($order_data['billing']['name'] == '') {
            $order_data['billing'] = $order_data['delivery'];
        }
        if ($order_data['delivery']['name'] == '') {
            $order_data['delivery'] = $order_data['billing'];
        }
        return $order_data;
    }

    public function generateOrderItemData($order, $taxRate)
    {
        $order_products = $this->db->query("SELECT op.*, p.tax_class_id, p.b1_reference_id FROM " . DB_PREFIX . "order_product op LEFT OUTER JOIN " . DB_PREFIX . "product p ON p.product_id = op.product_id WHERE order_id = " . $this->db->escape($order['order_id']));
        if (count($order_products->rows) === 0) {
            return 'skip';
        }

        $items = [];
        foreach ($order_products->rows as $key => $product) {
            $items[$key]['itemId'] = $product['b1_reference_id'];// @TODO code
            $items[$key]['name'] = $this->orders->getDataBySql($this->settings->get('items_name'), $product['order_product_id']);
            $items[$key]['quantity'] = round($this->orders->getDataBySql($this->settings->get('items_quantity'), $product['order_product_id']) * 1, 3);
            if ($product['tax_class_id'] > 0){
                if ($this->settings->get('items_vat_rate')){
                    $items[$key]['vatRate'] = (int)$this->orders->getDataBySql($this->settings->get('items_vat_rate'), $product['order_product_id']);;
                } else {
                    if (!is_null($taxRate)){
                        $items[$key]['vatRate'] = (int)$taxRate;
                    }
                }
            }
            $items[$key]['price'] = round(($this->orders->getDataBySql($this->settings->get('items_price'), $product['order_product_id']) + $product['tax']) * $order['currency_value'], 4);
            $items[$key]['sum'] = round(($this->orders->getDataBySql($this->settings->get('items_sum'), $product['order_product_id']) + ($product['tax'] * $product['quantity'])) * $order['currency_value'], 2);
        }
        return $items;
    }
    /**
     * @throws B1Exception
     */
    public function importItemsToB1($attributeId, $measurementId)
    {
        $this->library = new B1([
            'apiKey' => $this->settings->get('api_key'),
            'privateKey' => $this->settings->get('private_key')
        ]);
        $i = 0;
        $k = 0;
        $success = 0;
        $fail = 0;
        $errors = [];
        do {
            $i++;
            $items = $this->items->fetchAllItems(self::ITEMS_PER_ITERATION);
            foreach ($items as $item) {
                $item_data = $this->generate_item_data($item, $attributeId, $measurementId);
                try {
                    $k++;
                    $response = $this->library->request('reference-book/items/create', $item_data['data']);
                    $success++;
                    $content = $response->getContent();
                    $this->items->updateProductReferenceId($content['data']['id'], $item['sku']);
                } catch (B1Exception $e) {
                    $fail++;
                    $content = $e->getResponse()->getContent();
                    $errors['errorMessages'][$k] = [
                        $content['errors']['code'][0],
                    ];
                }
            }
            $errors['success'] = [
                'success' => $success,
            ];
            $errors['failed'] = [
                'failed' => $fail,
            ];
        } while (count($items) == self::ITEMS_PER_ITERATION && $i < self::MAX_ITERATIONS);
        print_r(json_encode($errors));
        die;
    }

    private function generate_item_data($item, $attributeId, $measurementId)
    {
        $item_data = [];
        $item_data['code'] = $item['sku'];
        $item_data['name'] = $item['name'];
        $item_data['attributeId'] = $attributeId;
        $item_data['measurementUnitId'] = $measurementId;
        $item_data['priceWithoutVat'] = round($item['price'], 2);

        return [
            'data' => $item_data
        ];
    }
}
