<?php

require_once(DIR_SYSTEM . 'library/vendor/b1/libb1/B1.php');
require_once(DIR_SYSTEM . 'library/db.php');
require_once(DIR_SYSTEM . 'library/db/' . DB_DRIVER . '.php');

class ControllerExtensionModuleB1Accounting extends Controller
{
    /**
     * @return array
     * @throws B1CronException
     * @throws B1Exception
     */
    public function index()
    {
        /* Load language file. */
        $this->load->language('module/b1accounting');
        $this->load->model('b1/settings');
        $this->load->model('b1/orders');
        $this->load->model('b1/items');
        $this->load->model('b1/manager');

        $this->load->model('catalog/product');

        $this->document->setTitle($this->language->get('heading_title'));
        /* Check if data has been posted back. */
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if (isset($this->request->post['getDropdownItems']) && $this->request->post['getDropdownItems'] == true) {
                $manager = new ModelB1Manager();
                $data = $manager->getImportDropdownItems();
                echo json_encode($data);
                die;
            }
            if ($this->request->post['importProducts'] == true) {
                $manager = new ModelB1Manager();
                $manager->importItemsToB1($this->request->post['attributeId'], $this->request->post['measurementId']);
                die;

            }
            if ($this->request->post['form'] == 'configuration') {
                $check = [
                    'shop_id',
                    'vat_rate',
                    'sync_invoice',
                    'quantity_sync',
                    'sync_item_name',
                    'sync_item_price',
                    'write_off',
                    'orders_sync_from',
                    'order_status_id',
                    'api_key',
                    'private_key',
                    'cron_key',
                ];
                $this->model_b1_settings->update($check, $this->request->post);
            }
            if ($this->request->post['mappingForm'] == 'mapping') {
                $check = [
                    'order_date',
                    'order_no',
                    'invoice_number',
                    'invoice_series',
                    'currency',
                    'discount',
                    'discountVat',
                    'gift',
                    'total',
                    'order_email',
                    'billing_is_company',
                    'billing_first_name',
                    'billing_last_name',
                    'billing_address',
                    'billing_city',
                    'billing_country',
                    'billing_short_name',
                    'billing_vat_code',
                    'billing_code',
                    'billing_postcode',
                    'delivery_is_company',
                    'delivery_first_name',
                    'delivery_last_name',
                    'delivery_address',
                    'delivery_city',
                    'delivery_country',
                    'delivery_short_name',
                    'delivery_vat_code',
                    'delivery_code',
                    'delivery_postcode',
                    'payer_name',
                    'payer_code',
                    'payer_vat_code',
                    'payer_address',
                    'payer_country_code',
                    'payment_code',
                    'payment_id',
                    'payment_payment_date',
                    'payment_sum',
                    'payment_tax',
                    'payment_currency',
                    'payment_payment',
                    'shipping_amount',
                    'shipping_amount_tax',
                    'items_name',
                    'items_vat_rate',
                    'items_quantity',
                    'items_price',
                    'items_price_vat',
                ];
                $this->model_b1_settings->update($check, $this->request->post);
            }
            if ($this->request->post['resetOrders'] == 'resetB1ReferenceId') {
                $this->model_b1_settings->resetOrdersId();
            }
            $this->response->redirect($this->url->link('extension/module/b1accounting', 'token=' . $this->session->data['token'], 'SSL'));
        }

        /* Load language strings. */
        $data['label_confirmed_status'] = $this->language->get('label_confirmed_status');
        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_help_page'] = $this->language->get('button_help_page');
        $data['button_documentation'] = $this->language->get('button_documentation');
        $data['button_fetch_items_from_b1'] = $this->language->get('button_fetch_items_from_b1');
        $data['button_sync_orders_without_writeoff'] = $this->language->get('button_sync_orders_without_writeoff');
        $data['button_update'] = $this->language->get('button_update');
        $data['reset_b1_reference_id'] = $this->language->get('reset_b1_reference_id');
        $data['button_contact'] = $this->language->get('button_contact');
        $data['label_settings'] = $this->language->get('label_settings');
        $data['label_failed_to_sync_orders'] = $this->language->get('label_failed_to_sync_orders');
        $data['label_eshop_id'] = $this->language->get('label_eshop_id');
        $data['label_b1_id'] = $this->language->get('label_b1_id');
        $data['label_eshop_name'] = $this->language->get('label_eshop_name');
        $data['label_b1_name'] = $this->language->get('label_b1_name');
        $data['label_b1_code'] = $this->language->get('label_b1_code');
        $data['label_sync_status'] = $this->language->get('label_sync_status');
        $data['label_items_in_eshop'] = $this->language->get('label_items_in_eshop');
        $data['label_items_in_b1'] = $this->language->get('label_items_in_b1');
        $data['label_orders_failed_to_sync'] = $this->language->get('label_orders_failed_to_sync');
        $data['title_eshop_items'] = $this->language->get('title_eshop_items');
        $data['title_b1_items'] = $this->language->get('title_b1_items');
        $data['text_configuration'] = $this->language->get('text_configuration');
        $data['text_cron_description_products'] = $this->language->get('text_cron_description_products');
        $data['text_cron_description_orders'] = $this->language->get('text_cron_description_orders');
        $data['text_link'] = $this->language->get('text_link');
        $data['text_settings_save_success'] = $this->language->get('text_settings_save_success');
        $data['text_cron'] = $this->language->get('text_cron');
        $data['run_cron'] = $this->language->get('run_cron');
        $data['text_status'] = $this->language->get('text_status');
        $data['cron_urls'] = $this->language->get('cron_urls');
        $data['label_id'] = $this->language->get('label_id');
        $data['label_name'] = $this->language->get('label_name');
        $data['label_code'] = $this->language->get('label_code');
        $data['label_shop_identifier'] = $this->language->get('label_shop_identifier');
        $data['orders_sync_from'] = $this->language->get('orders_sync_from');
        $data['order_status_id'] = $this->language->get('order_status_id');
        $data['label_customer'] = $this->language->get('label_customer');
        $data['label_product_code'] = $this->language->get('label_product_code');
        $data['label_total'] = $this->language->get('label_total');
        $data['label_status'] = $this->language->get('label_status');
        $data['reset_all'] = $this->language->get('reset_all');
        $data['reset_all_orders'] = $this->language->get('reset_all_orders');
        $data['label_quantity_sync'] = $this->language->get('label_quantity_sync');
        $data['label_sync_item_name'] = $this->language->get('label_sync_item_name');
        $data['label_sync_item_price'] = $this->language->get('label_sync_item_price');
        $data['label_vat_rate'] = $this->language->get('label_vat_rate');
        $data['label_invoice_number_length'] = $this->language->get('label_invoice_number_length');
        $data['label_invoice_series'] = $this->language->get('label_invoice_series');
        $data['label_sync_invoice'] = $this->language->get('label_sync_invoice');
        $data['label_order_date'] = $this->language->get('label_order_date');
        $data['label_order_no'] = $this->language->get('label_order_no');
        $data['label_invoice_number'] = $this->language->get('label_invoice_number');
        $data['label_currency'] = $this->language->get('label_currency');
        $data['label_discount'] = $this->language->get('label_discount');
        $data['label_discountVat'] = $this->language->get('label_discountVat');
        $data['label_gift'] = $this->language->get('label_gift');
        $data['label_total'] = $this->language->get('label_total');
        $data['label_order_email'] = $this->language->get('label_order_email');
        $data['label_billing_is_company'] = $this->language->get('label_billing_is_company');
        $data['label_billing_first_name'] = $this->language->get('label_billing_first_name');
        $data['label_billing_last_name'] = $this->language->get('label_billing_last_name');
        $data['label_billing_address'] = $this->language->get('label_billing_address');
        $data['label_billing_city'] = $this->language->get('label_billing_city');
        $data['label_billing_country'] = $this->language->get('label_billing_country');
        $data['label_billing_short_name'] = $this->language->get('label_billing_short_name');
        $data['label_billing_vat_code'] = $this->language->get('label_billing_vat_code');
        $data['label_billing_code'] = $this->language->get('label_billing_code');
        $data['label_billing_postcode'] = $this->language->get('label_billing_postcode');
        $data['label_delivery_is_company'] = $this->language->get('label_delivery_is_company');
        $data['label_delivery_first_name'] = $this->language->get('label_delivery_first_name');
        $data['label_delivery_last_name'] = $this->language->get('label_delivery_last_name');
        $data['label_delivery_address'] = $this->language->get('label_delivery_address');
        $data['label_delivery_city'] = $this->language->get('label_delivery_city');
        $data['label_delivery_country'] = $this->language->get('label_delivery_country');
        $data['label_delivery_short_name'] = $this->language->get('label_delivery_short_name');
        $data['label_delivery_vat_code'] = $this->language->get('label_delivery_vat_code');
        $data['label_delivery_code'] = $this->language->get('label_delivery_code');
        $data['label_delivery_postcode'] = $this->language->get('label_delivery_postcode');
        $data['label_payer_name'] = $this->language->get('label_payer_name');
        $data['label_payer_code'] = $this->language->get('label_payer_code');
        $data['label_payer_vat_code'] = $this->language->get('label_payer_vat_code');
        $data['label_payer_address'] = $this->language->get('label_payer_address');
        $data['label_payer_country_code'] = $this->language->get('label_payer_country_code');
        $data['label_payment_code'] = $this->language->get('label_payment_code');
        $data['label_payment_id'] = $this->language->get('label_payment_id');
        $data['label_payment_payment_date'] = $this->language->get('label_payment_payment_date');
        $data['label_payment_sum'] = $this->language->get('label_payment_sum');
        $data['label_payment_tax'] = $this->language->get('label_payment_tax');
        $data['label_payment_currency'] = $this->language->get('label_payment_currency');
        $data['label_payment_payment'] = $this->language->get('label_payment_payment');
        $data['label_shipping_amount'] = $this->language->get('label_shipping_amount');
        $data['label_shipping_amount_tax'] = $this->language->get('label_shipping_amount_tax');
        $data['label_items_name'] = $this->language->get('label_items_name');
        $data['label_items_vat_rate'] = $this->language->get('label_items_vat_rate');
        $data['label_items_quantity'] = $this->language->get('label_items_quantity');
        $data['label_items_price'] = $this->language->get('label_items_price');
        $data['label_items_price_vat'] = $this->language->get('label_items_price_vat');
        $data['url_cancel'] = $this->url->link('extension/extension', 'token = ' . $this->session->data['token'], 'SSL');
        $data['url_documentation'] = $this->model_b1_settings->get('documentation_url');
        $data['url_help_page'] = $this->model_b1_settings->get('help_page_url');
        $data['url_contact'] = $this->model_b1_settings->get('b1_contact_email');
        $data['text_import_products'] = $this->language->get('text_import_products');
        $data['import_error'] = $this->language->get('import_error');
        $data['token'] = $this->session->data['token'];
        $data['label_logs'] = $this->language->get('label_logs');
        $data['label_export'] = $this->language->get('label_export');

        /* Loading up some URLS. */
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $data['form_action'] = $this->url->link('extension/module/b1accounting', 'token=' . $this->session->data['token'], 'SSL');
        $data['reset_order_action'] = $this->url->link('extension/module/b1accounting', 'token=' . $this->session->data['token'], 'SSL');

        /* Present error messages to users. */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        /* Breadcrumb. */
        $data['breadcrumbs'] = [];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token = ' . $this->session->data['token'], 'SSL')
        ];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/extension', 'token = ' . $this->session->data['token'], 'SSL')
        ];

        $data['breadcrumbs'][] = [
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/b1accounting', 'token = ' . $this->session->data['token'], 'SSL')
        ];

        /* Crons urls */
        $data['cron_urls'] = [
            'fetchItemsFromB1' => [
                'url' => $this->model_b1_settings->cronUrl('products'),
            ],
            'syncOrdersWithB1' => [
                'url' => $this->model_b1_settings->cronUrl('orders'),
            ],
        ];

        /* Render some output. */
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['settings'] = $this->load->controller('module/b1/settings');
        $data['fail'] = $this->load->controller('module/b1/fail');

        /* B1 settings */
        $data['settings'] = [
            [
                'type' => 'text',
                'name' => 'shop_id',
                'label' => $this->language->get('label_shop_identifier'),
                'value' => $this->model_b1_settings->get('shop_id'),
            ],
            [
                'type' => 'text',
                'name' => 'orders_sync_from',
                'label' => $this->language->get('orders_sync_from'),
                'value' => $this->model_b1_settings->get('orders_sync_from'),
            ],
            [
                'type' => 'text',
                'name' => 'cron_key',
                'label' => $this->language->get('label_cron_key'),
                'value' => $this->model_b1_settings->get('cron_key'),
            ],
            [
                'type' => 'text',
                'name' => 'api_key',
                'label' => $this->language->get('label_api_key'),
                'value' => $this->model_b1_settings->get('api_key'),
            ],
            [
                'type' => 'text',
                'name' => 'private_key',
                'label' => $this->language->get('label_private_key'),
                'value' => $this->model_b1_settings->get('private_key'),
            ],
            [
                'type' => 'text',
                'name' => 'vat_rate',
                'label' => $this->language->get('label_vat_rate'),
                'value' => $this->model_b1_settings->get('vat_rate'),
            ],
            [
                'type' => 'select',
                'name' => 'sync_invoice',
                'label' => $this->language->get('label_sync_invoice'),
                'value' => $this->model_b1_settings->get('sync_invoice'),
            ],
            [
                'type' => 'select',
                'name' => 'quantity_sync',
                'label' => $this->language->get('label_quantity_sync'),
                'value' => $this->model_b1_settings->get('quantity_sync'),
            ],
            [
                'type' => 'select',
                'name' => 'sync_item_name',
                'label' => $this->language->get('label_sync_item_name'),
                'value' => $this->model_b1_settings->get('sync_item_name'),
            ],
            [
                'type' => 'select',
                'name' => 'sync_item_price',
                'label' => $this->language->get('label_sync_item_price'),
                'value' => $this->model_b1_settings->get('sync_item_price'),
            ],
            [
                'type' => 'select',
                'name' => 'write_off',
                'label' => $this->language->get('label_write_off'),
                'value' => $this->model_b1_settings->get('write_off'),
            ],
            [
                'type' => 'selectOrderConfirmId',
                'name' => 'order_status_id',
                'label' => $this->language->get('order_status_id'),
                'value' => $this->model_b1_settings->get('order_status_id'),
            ],
        ];
        $data['mapping'] = [
            [
                'type' => 'text',
                'name' => 'order_date',
                'label' => $this->language->get('label_order_date'),
                'value' => $this->model_b1_settings->get('order_date'),
            ],
            [
                'type' => 'text',
                'name' => 'order_no',
                'label' => $this->language->get('label_order_no'),
                'value' => $this->model_b1_settings->get('order_no'),
            ],
            [
                'type' => 'text',
                'name' => 'invoice_series',
                'label' => $this->language->get('label_invoice_series'),
                'value' => $this->model_b1_settings->get('invoice_series'),
            ],
            [
                'type' => 'text',
                'name' => 'invoice_number',
                'label' => $this->language->get('label_invoice_number'),
                'value' => $this->model_b1_settings->get('invoice_number'),
            ],
            [
                'type' => 'text',
                'name' => 'currency',
                'label' => $this->language->get('label_currency'),
                'value' => $this->model_b1_settings->get('currency'),
            ],
            [
                'type' => 'text',
                'name' => 'shipping_amount',
                'label' => $this->language->get('label_shipping_amount'),
                'value' => $this->model_b1_settings->get('shipping_amount'),
            ],
            [
                'type' => 'text',
                'name' => 'discount',
                'label' => $this->language->get('label_discount'),
                'value' => $this->model_b1_settings->get('discount'),
            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('discountVat'),
//            ],
            [
                'type' => 'text',
                'name' => 'gift',
                'label' => $this->language->get('label_gift'),
                'value' => $this->model_b1_settings->get('gift'),
            ],
            [
                'type' => 'text',
                'name' => 'total',
                'label' => $this->language->get('label_total'),
                'value' => $this->model_b1_settings->get('total'),
            ],
            [
                'type' => 'text',
                'name' => 'order_email',
                'label' => $this->language->get('label_order_email'),
                'value' => $this->model_b1_settings->get('order_email'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_is_company',
                'label' => $this->language->get('label_billing_is_company'),
                'value' => $this->model_b1_settings->get('billing_is_company'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_first_name',
                'label' => $this->language->get('label_billing_first_name'),
                'value' => $this->model_b1_settings->get('billing_first_name'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_last_name',
                'label' => $this->language->get('label_billing_last_name'),
                'value' => $this->model_b1_settings->get('billing_last_name'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_address',
                'label' => $this->language->get('label_billing_address'),
                'value' => $this->model_b1_settings->get('billing_address'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_city',
                'label' => $this->language->get('label_billing_city'),
                'value' => $this->model_b1_settings->get('billing_city'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_country',
                'label' => $this->language->get('label_billing_country'),
                'value' => $this->model_b1_settings->get('billing_country'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_short_name',
                'label' => $this->language->get('label_billing_short_name'),
                'value' => $this->model_b1_settings->get('billing_short_name'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_vat_code',
                'label' => $this->language->get('label_billing_vat_code'),
                'value' => $this->model_b1_settings->get('billing_vat_code'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_code',
                'label' => $this->language->get('label_billing_code'),
                'value' => $this->model_b1_settings->get('billing_code'),
            ],
            [
                'type' => 'text',
                'name' => 'billing_postcode',
                'label' => $this->language->get('label_billing_postcode'),
                'value' => $this->model_b1_settings->get('billing_postcode'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_is_company',
                'label' => $this->language->get('label_delivery_is_company'),
                'value' => $this->model_b1_settings->get('delivery_is_company'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_first_name',
                'label' => $this->language->get('label_delivery_first_name'),
                'value' => $this->model_b1_settings->get('delivery_first_name'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_last_name',
                'label' => $this->language->get('label_delivery_last_name'),
                'value' => $this->model_b1_settings->get('delivery_last_name'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_address',
                'label' => $this->language->get('label_delivery_address'),
                'value' => $this->model_b1_settings->get('delivery_address'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_city',
                'label' => $this->language->get('label_delivery_city'),
                'value' => $this->model_b1_settings->get('delivery_city'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_country',
                'label' => $this->language->get('label_delivery_country'),
                'value' => $this->model_b1_settings->get('delivery_country'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_short_name',
                'label' => $this->language->get('label_delivery_short_name'),
                'value' => $this->model_b1_settings->get('delivery_short_name'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_vat_code',
                'label' => $this->language->get('label_delivery_vat_code'),
                'value' => $this->model_b1_settings->get('delivery_vat_code'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_code',
                'label' => $this->language->get('label_delivery_code'),
                'value' => $this->model_b1_settings->get('delivery_code'),
            ],
            [
                'type' => 'text',
                'name' => 'delivery_postcode',
                'label' => $this->language->get('label_delivery_postcode'),
                'value' => $this->model_b1_settings->get('delivery_postcode'),
            ],
//            [
//                'type' => 'text',
//                'name' => 'payer_name',
//                'label' => $this->language->get('label_payer_name'),
//                'value' => $this->model_b1_settings->get('payer_name'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'payer_code',
//                'label' => $this->language->get('label_payer_code'),
//                'value' => $this->model_b1_settings->get('payer_code'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'payer_vat_code',
//                'label' => $this->language->get('label_payer_vat_code'),
//                'value' => $this->model_b1_settings->get('payer_vat_code'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'payer_address',
//                'label' => $this->language->get('label_payer_address'),
//                'value' => $this->model_b1_settings->get('payer_address'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'payer_country_code',
//                'label' => $this->language->get('label_payer_country_code'),
//                'value' => $this->model_b1_settings->get('payer_country_code'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'payment_code',
//                'label' => $this->language->get('label_payment_code'),
//                'value' => $this->model_b1_settings->get('payment_code'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('payment_id'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('payment_payment_date'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('payment_sum'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('payment_tax'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('payment_currency'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('payment_payment'),
//            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('shipping_amount_tax'),
//            ],
            [
                'type' => 'text',
                'name' => 'items_name',
                'label' => $this->language->get('label_items_name'),
                'value' => $this->model_b1_settings->get('items_name'),
            ],
            [
                'type' => 'text',
                'name' => 'items_vat_rate',
                'label' => $this->language->get('label_items_vat_rate'),
                'value' => $this->model_b1_settings->get('items_vat_rate'),
            ],
            [
                'type' => 'text',
                'name' => 'items_quantity',
                'label' => $this->language->get('label_items_quantity'),
                'value' => $this->model_b1_settings->get('items_quantity'),
            ],
            [
                'type' => 'text',
                'name' => 'items_price',
                'label' => $this->language->get('label_items_price'),
                'value' => $this->model_b1_settings->get('items_price'),
            ],
//            [
//                'type' => 'text',
//                'name' => 'shop_id',
//                'label' => $this->language->get('label_shop_identifier'),
//                'value' => $this->model_b1_settings->get('items_price_vat'),
//            ],
        ];
        $data['init_quantity'] = $this->model_b1_settings->get('quantity_sync');
        $languageCode = $this->config->get('config_language_id');
        $data['order_statuses'] = $this->model_b1_orders->getOrderStatuses($languageCode);

        $this->migration();

        $this->response->setOutput($this->load->view('module/b1accounting.tpl', $data));
    }

    public function install()
    {
        $this->load->model('setting/setting');
        $this->load->model('b1/settings');
        $this->load->model('b1/items');
        $this->load->model('b1/logs');

        $this->model_b1_settings->createTable();
        $this->model_b1_settings->insertDefaultSettings();
        $this->model_b1_settings->updateExternalTables();

        $this->model_b1_logs->createTable();

        $settings = $this->model_setting_setting->getSetting('b1');
        $settings['b1_status'] = 1;
        $this->model_setting_setting->editSetting('b1', $settings);
    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->load->model('b1/settings');
        $this->load->model('b1/items');
        $this->load->model('b1/logs');

        $this->model_b1_settings->deleteTable();
        $this->model_b1_settings->deleteExternalTableModifications();

        $this->model_b1_logs->deleteTable();

        $settings = $this->model_setting_setting->getSetting('b1');
        $settings['b1_status'] = 0;
        $this->model_setting_setting->editSetting('b1', $settings);
    }

    public function ajax()
    {
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['ajax'])) {
            $this->load->model('b1/logs');
            $action = $this->request->post['action'];
            switch ($action) {
                case 'LoadLogs':
                    $page = isset($this->request->post['page']) ? (int)$this->request->post['page'] : 1;
                    $response = $this->model_b1_logs->fetchAllLogsAsHtml($page);
                    break;
                case 'ViewDetailLog':
                    $id = (int)$this->request->post['id'];
                    $response = $this->model_b1_logs->fetchLogByIdAsHtml($id);
                    break;
                default:
                    $response = 'Something goes wrong';
                    break;
            }
        }
        $this->response->setOutput($response);
    }

    public function export()
    {
        if (isset($this->request->post['ajax']) && isset($this->request->post['action']) == 'ExportLogs') {
            $this->load->model('b1/logs');

            $selected = $this->request->post['selected'];
            $export_data = $this->model_b1_logs->exportLogs($selected);
            $filename = 'b1-opencart-logs-' . date('Y_m_d_H_i_s');

            $this->response->addHeader('Content-type: text/plain');
            $this->response->addHeader('Content-Disposition: attachment; filename="' . $filename . '.json"');
            $this->response->setOutput($export_data);
        } else {
            $this->response->setOutput('Something goes wrong');
        }

    }

    private function migration()
    {
        $this->load->model('setting/setting');
        $settings = $this->model_setting_setting->getSetting('b1');
        $installed_version = isset($settings['b1_database_version']) ? intval($settings['b1_database_version']) : 0;

        if($installed_version < 1) { // log table
            $this->load->model('b1/logs');
            $this->model_b1_logs->createTable();

            $settings['b1_database_version'] = 1;
            $this->model_setting_setting->editSetting('b1', $settings);
        }

        if($installed_version < 2) { // new mappings
            $this->load->model('b1/settings');
            $this->model_b1_settings->add('billing_code', '');
            $this->model_b1_settings->add('delivery_code', '');
            $settings['b1_database_version'] = 2;
            $this->model_setting_setting->editSetting('b1', $settings);
        }

    }

}
