<?= $header; ?>
<link type="text/css" href="view/stylesheet/b1accounting.css" rel="stylesheet">
<?= $column_left; ?>

<div id="content" class="b1-extension">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a
                        title="Version - " target="_blank" class="btn btn-default">
                    <i class="fa fa-book"></i> <span class="hidden-xs">Version - <?php $version=new B1(['apiKey'=>' ','privateKey'=>' ']); echo $version::VERSION; ?></span>
                </a>
                <a href="mailto:<?= $url_contact; ?>" data-toggle="tooltip"
                   title="<?= $button_contact; ?>" class="btn btn-default">
                    <i class="fa fa-envelope"></i>
                </a>
                <a href="<?= $url_documentation; ?>" data-toggle="tooltip"
                   title="<?= $button_documentation; ?>" target="_blank" class="btn btn-default">
                    <i class="fa fa-book"></i> <span class="hidden-xs"><?= $button_documentation; ?></span>
                </a>
                <a href="<?= $url_help_page; ?>" data-toggle="tooltip" title="<?= $button_help_page; ?>"
                   target="_blank" class="btn btn-default">
                    <i class="fa fa-book"></i> <span class="hidden-xs"><?= $button_help_page ?></span>
                </a>
                <a href="<?= $url_cancel; ?>" data-toggle="tooltip" title="<?= $button_cancel; ?>"
                   class="btn btn-default">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h1><?= $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?= $breadcrumb['href']; ?>"><?= $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#settings" data-toggle="tab"><span class="text-primary"><i class="fa fa-cogs"></i></span> <span
                            class="text-primary hidden-xs"><?= $label_settings; ?></span></a></li>
            <li><a href="#mapping" data-toggle="tab"><span class="text-success"><i class="fa fa-compress"></i></span> <span
                            class="text-success hidden-xs"><?= 'Field mapping';//$label_unlinked_items; ?></span></a></li>
            <li><a href="#logs" data-toggle="tab"><span class="text-warning"><i class="fa fa-list"></i></span> <span
                            class="text-warning hidden-xs"><?= $label_logs; ?></span></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cogs"></i> <?= $text_configuration; ?>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <form action="<?= $form_action; ?>" method="post" class="form-horizontal">
                                    <input type="hidden" name="form" value="configuration">
                                    <div class="alert alert-success hidden" id="success-msg" role="alert">
                                        <button type="button" class="close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        <i class="fa fa-check"></i> <?= $text_settings_save_success; ?>
                                    </div>
                                    <?php foreach ($settings as $data) { ?>
                                    <div class="form-group">
                                        <?php if ($data['type'] == 'text') { ?>
                                        <label class="col-sm-5 control-label"
                                               for="<?= $data['name']; ?>"><?= $data['label']; ?>:</label>
                                        <div class="col-sm-7">
                                            <input class="form-control" name="<?= $data['name']; ?>" type="text"
                                                   value="<?= $data['value']; ?>"
                                                   placeholder="<?= $data['label']; ?>">
                                            <div class="error-message"></div>
                                        </div>
                                        <?php } if( $data['type'] == 'select') { ?>
                                        <label class="col-sm-5 control-label"
                                               for="<?= $data['name']; ?>"><?= $data['label']; ?>:</label>
                                        <div class="col-sm-7">
                                            <select class="form-control" name="<?= $data['name']; ?>"
                                                    placeholder="<?= $data['label']; ?>">
                                                <option value="1"
                                                <?php if($data['value'] == '1'){ echo 'selected';} ?>
                                                ><?= 'Enable'; ?></option>
                                                <option value="0"
                                                <?php if($data['value'] == '0'){ echo 'selected';} ?>
                                                ><?= 'Disable'; ?></option>
                                            </select>
                                            <div class="error-message"></div>
                                        </div>
                                        <?php } elseif( $data['type'] == 'selectOrderConfirmId') { ?>
                                        <label class="col-sm-5 control-label"
                                               for="<?= $data['name']; ?>"><?= $data['label']; ?>:</label>
                                        <div class="col-sm-7">
                                            <select name="<?= $data['name']; ?>" id="input-order-status"
                                                    class="form-control">
                                                <?php foreach ($order_statuses as $order_status) { ?>
                                                <?php $order_status['order_status_id'] ?>
                                                <option value="<?= $order_status['order_status_id']; ?>"
                                                <?= $order_status['order_status_id'] == $data['value'] ? "selected" : ""?>
                                                ><?= $order_status['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary"><i
                                                        class="fa fa-save"></i> <?= $button_update; ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                <form action="<?= $reset_order_action; ?>" method="post">
                                    <input type="hidden" name="resetOrders" value="resetB1ReferenceId"/>
                                    <button type="submit" class="btn btn-primary"><i
                                                class="fa fa-minus-circle"></i> <?= $reset_b1_reference_id; ?></button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-repeat"></i> <?= $text_cron; ?></h3>
                            </div>
                            <div class="list-group">
                                <?php foreach ($cron_urls as $name => $data) { ?>
                                <div class="list-group-item">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="<?= $data['url'] ?>">
                                        <div class="input-group-btn">
                                            <a href="<?= $data['url'] ?>" type="button " target="_blank"
                                               class="btn btn-primary"><i class="fa fa-play-circle"
                                                                          aria-hidden="true"></i> <?= $run_cron; ?>
                                            </a>
                                        </div>
                                    </div>
                                    <?php
                                    if ($name == 'syncOrdersWithB1'){
                                    echo $text_cron_description_orders;
                                    }
                                    if ($name == 'fetchItemsFromB1'){
                                    echo $text_cron_description_products;
                                    }
                                    ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <button class="btn btn-primary" role="button" onclick="getDropdownData()" id="getImportDropdownItems" data-toggle="collapse" data-parent="#accordion"
                                data-target="#collapseOne"
                                aria-expanded="false"
                                aria-controls="collapseOne">
                            <i class="fas fa-cloud-upload-alt"></i><?= $text_import_products ?>
                        </button>
                        <form onsubmit="importItemsToB1()" id="formData">
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="fa fa-cloud-upload"></i> <?= $text_import_products ?>
                                        </h3>
                                    </div>
                                    <div class="list-group">
                                        <div class="list-group-item">
                                            <div class="form-group">
                                                <label for="attribute_id"></label>
                                                <select class="form-control" id="attribute_id">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="measurement_unit_id"></label>
                                                <select class="form-control" id="measurement_unit_id">
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" class="btn btn-primary">
                                                <i class="fas fa-cloud-upload-alt"></i>
                                                </input>
                                            </div>
                                            <div class="alert alert-info">
                                                <!--<i><?= $import_error ?></i> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="mapping">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-compress"></i> <?= 'Field mapping';//$text_configuration; ?>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <form action="<?= $form_action; ?>" method="post" class="form-horizontal">
                                    <input type="hidden" name="mappingForm" value="mapping">
                                    <div class="alert alert-success hidden" id="success-msg" role="alert">
                                        <button type="button" class="close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        <i class="fa fa-check"></i> <?= $text_settings_save_success; ?>
                                    </div>
                                    <?php foreach ($mapping as $data) { ?>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label" for="<?= $data['name']; ?>"><?= $data['label']; ?>:</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" name="<?= $data['name']; ?>" type="text" rows="4"
                                                      placeholder="<?= $data['label']; ?>"><?= $data['value']; ?></textarea>
                                            <div class="error-message"></div>
                                        </div>
                                    </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary"><i
                                                        class="fa fa-save"></i> <?= $button_update; ?></button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="logs">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-7">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">
                                    <i class="fas fa-cogs"></i> <?= $label_logs; ?>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive" id="b1TableLogs">
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-5">
                        <div>
                            <div class="margin-bottom-15">
                                <button type="submit" class="btn btn-primary" id="b1DownloadBtn">
                                    <i class="fas fa-cloud-download-alt"></i> <?= $label_export; ?>
                                </button>
                                <span id="b1DownloadLink" class="margin-left-15"></span>
                            </div>
                            <div id="b1LogDetailView"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript"
        src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js">
</script>
<script>

    function importItemsToB1() {
        event.preventDefault();
        var data = [];
        data.postName = {importProducts:true};
        data.attributeId = {attributeId:document.getElementById('formData')[0].value};
        data.measurementId = {measurementId:document.getElementById('formData')[1].value};
        $.ajax({
            url: 'index.php?route=extension/module/b1accounting&token=<?= $token?>',
            type: 'POST',
            data: {importProducts:true,attributeId:document.getElementById('formData')[0].value,measurementId:document.getElementById('formData')[1].value},
        }).done(function (response) {
            var responseObj = JSON.parse(response);
            if (responseObj.failed.failed == 0) {
                alert('All products imported successfully');
            } else {
                alert('Products failed to import to B1 - ' + responseObj.failed.failed + '\n' + 'Products successfully imported to B1 - '
                    + responseObj.success.success + '\n' + 'Error messages : ' + '\n' + JSON.stringify(responseObj.errorMessages, null, 2));
            }
            debugger;
        }).fail(function (response) {

        });
    }

    function getDropdownData() {
        $("#attribute_id").empty();
        $("#measurement_unit_id").empty();
        event.preventDefault();
        $('#getImportDropdownItems')
            .hide();
        var data = [];
        data.postName = {getDropdownItems:true};
        $.ajax({
            url: 'index.php?route=extension/module/b1accounting&token=<?= $token?>',
            type: 'POST',
            data: data.postName,
        }).done(function (response) {
            var obj = JSON.parse(response);
            $.each(obj.attributeId.data, function (index, value) {
                $('#attribute_id')
                    .prepend("<option value = " + value.id + ">" + value.name + "</option>")
                    .find('listItem');
            });
            $.each(obj.measurementId.data, function (index, value) {
                $('#measurement_unit_id')
                    .prepend("<option value = " + value.id + ">" + value.name + "</option>")
                    .find('listItem');
            });
        }).fail(function (response) {
            debugger;
        });
    }

    /**
     * Display table data
     */
    function b1LoadLogs(page) {
        $.ajax({
            url: 'index.php?route=extension/module/b1accounting/ajax&token=<?= $token?>',
            type: 'post',
            data: {
                ajax: true,
                page: page,
                action: 'LoadLogs',
            },
            success: function (response) {
                $('#b1TableLogs').html(response)
            }
        }).fail(function () {
            alert('Error')
        });
    }

    /**
     * View separate item
     * @param id
     */
    function b1ViewDetailLog(id) {
        $.ajax({
            url: 'index.php?route=extension/module/b1accounting/ajax&token=<?= $token?>',
            type: 'post',
            data: {
                ajax: true,
                action: 'ViewDetailLog',
                id: id
            },
            success: function (response) {
                $('#b1LogDetailView').html(response)
            }
        }).fail(function () {
            alert('Error')
        });
    }

    /**
     * Download file from php stream
     * @param selected_ids
     */
    function b1ExportLogs(selected_ids) {
        // for fucking old version of jQuery need this stuff
        $.ajaxTransport("+binary", function(options, originalOptions, jqXHR){
            // check for conditions and support for blob / arraybuffer response type
            if (window.FormData && ((options.dataType && (options.dataType == 'binary')) || (options.data && ((window.ArrayBuffer && options.data instanceof ArrayBuffer) || (window.Blob && options.data instanceof Blob)))))
            {
                return {
                    send: function(headers, callback){
                        var xhr = new XMLHttpRequest(),
                            url = options.url,
                            type = options.type,
                            async = options.async || true,
                            dataType = options.responseType || "blob",
                            data = options.data || null,
                            username = options.username || null,
                            password = options.password || null;

                        xhr.addEventListener('load', function(){
                            var data = {};
                            data[options.dataType] = xhr.response;
                            callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
                        });

                        xhr.open(type, url, async, username, password);

                        for (var i in headers ) {
                            xhr.setRequestHeader(i, headers[i] );
                        }

                        xhr.responseType = dataType;
                        xhr.send(Object.entries(data).map(([key, val]) => `${key}=${val}`).join('&'));
                    },
                    abort: function(){
                        jqXHR.abort();
                    }
                };
            }
        });
        $.ajax({
            url: 'index.php?route=extension/module/b1accounting/export&token=<?= $token?>',
            type: 'post',
            dataType: 'binary',
            responseType: 'blob',
            processData: false,
            data: {
                ajax: true,
                action: 'ExportLogs',
                selected: selected_ids
            },
            success: function (data, status, xhr) {
                var link = document.createElement('a'),
                    filename = 'b1-prestashop-logs.json';
                if (xhr.getResponseHeader('Content-Disposition')) { // filename from php
                    filename = xhr.getResponseHeader('Content-Disposition');
                    filename = filename.match(/filename="(.*?)"/)[1];
                    filename = decodeURIComponent(escape(filename));
                }
                link.href = URL.createObjectURL(data);
                link.download = filename;
                var text = document.createTextNode(filename);
                link.appendChild(text);
                $('#b1DownloadLink').append(link);
            }
        }).fail(function () {
            alert('Error')
        });
    }

    /**
     * Refresh log tab on show it
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if (target === '#logs') {
            b1LoadLogs(1);
        }
    });

    $(document).on("click", ".B1-logs tr", function (e) {
        var id = $(this).attr('data-id');
        b1ViewDetailLog(id);
    });

    $('#b1DownloadBtn').click(function (event) {
        event.preventDefault();
        $('#b1DownloadLink').html('');
        var selected_ids = [];
        $.each($("input[name='b1LogItemChkBox[]']:checked"), function () {
            selected_ids.push($(this).val());
        });
        b1ExportLogs(selected_ids);
    });

    $(document).on("click", "a.pagination_page_number", function (event) {
        event.preventDefault();
        var page = $(this).attr('rel');
        b1LoadLogs(page);
    });


</script>
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>
<?= $footer; ?>
