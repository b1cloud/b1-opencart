Įskiepis skirtas sinchronizuoti produktus tarp OpenCart ir B1.lt aplikacijos.

### Reikalavimai ###

* PHP 7.3
* OpenCart 2.3.0.2
* MySQL 5.7.15

### Diegimas 2.3.x versijoms ###

* NEBŪTINA Pasidarykite failų atsarginę kopiją.
* Padarykite atsarginę DB kopiją.
* Perkelkite `admin`, `system`, `catalog` direktorijas į opencart direktoriją.
* Perkelkite `b1accounting.php` failą į opencart `admin\controller\extension\module` direktoriją.
* Ištrinkite `admin\controller\module` aplankalą.
* Administracijos skiltyje `Extensions->extentions` pasirinkite iš išskleidžiamo meniu `Modules` įdiekite "B1 accounting" modulį ir suveskite reikiamą informaciją.
* Administracijos skiltyje, "B1 accounting" modulyje 'Orders sync from' laukelyje nurodykite data, nuo kurios bus sinchromnizuojami užsakymai. Datos formatas Y-m-d. Pvz. 2016-12-10 
* Administracijos skiltyje, "B1 accounting" modulyje 'Confirmed order status ID' laukelyje nurodykite užsakymų, kurie bus sinchronizuojami su B1, statuso ID. Pagal nutylėjimą 5 (Complete)
* Prie serverio Cron darbų sąrašo pridėkite visus išvardintus cron darbus, nurodytus modulio konfigūravimo puslapyje.
  Pridėti cron darbus galite per serverio valdymo panelė (DirectAdmin, Cpanel) arba įvykdę šias komandinės eilutės serverio pusėje
    * `0 */12 * * * wget -q -O - '[products_cron_url]'` Vietoj [products_cron_url] reikia nurodyti savo Cron adresą. 
    * `*/5 * * * * wget -q -O - '[orders_cron_url]'` Vietoj [orders_cron_url] reikia nurodyti savo Cron adresą. 
    * `0 */4 * * *  wget -q -O - '[updatedProducts_cron_url]'` Vietoj [quantities_cron_url] reikia nurodyti savo Cron adresą. 

### Pastabos 2.3.x versijoms ###

* Norėdami, kad pirkėjai matytų B1 sugeneruotas sąskaitas, reikia `catalog\view\theme\default\template\account\order_info.tpl` ( priklausomai nuo Jūsų temos vietoj `default` gali būti kitoks pavadinimas) faile įterpti norimoje puslapio vietoje nuorodą su $b1_invoice_link kintamuoju (PDF sąskaitos adresas) pvz. `<a target="_new" class="btn btn-default" href='<?php echo $b1_invoice_link ?>'>PDF</a>`
* Ištrinkite `admin/language/english` aplankalą.

### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt